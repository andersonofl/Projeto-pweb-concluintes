package br.edu.ifpb.pweb2.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Professor implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	private String matricula;
	
	private String nome;
	
	public Professor(){}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	@Override
	public String toString() {
		return "Professor [matricula=" + matricula + ", nome=" + nome + "]";
	}
	
}
