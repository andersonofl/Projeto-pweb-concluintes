package br.edu.ifpb.pweb2.util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import br.edu.ifpb.pweb2.dao.AlunoDAO;
import br.edu.ifpb.pweb2.model.Aluno;

@FacesConverter("alunoConverter")
public class AlunoConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String matricula) {
		
		if(matricula != null && matricula.trim().length() > 0) {
            try {
            	AlunoDAO service = new AlunoDAO();
            	return service.getById(Long.valueOf(matricula));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        }
        
		return null;
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
			if(object != null) {
	            return String.valueOf(((Aluno) object).getMatricula());
	        }
	 
            return null;
	}

}
