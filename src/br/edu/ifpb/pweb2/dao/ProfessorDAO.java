package br.edu.ifpb.pweb2.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.edu.ifpb.pweb2.model.Professor;

public class ProfessorDAO extends DAO<String,Professor>{

        
        @SuppressWarnings("unchecked")
	public List<Professor> getProfessoresByNome(String nome) {
		try {
			
			Query q = super.entityManager.createQuery("Select p from Professor p where lower(p.nome) like lower(:nome)");
			q.setParameter("nome", "%"+nome+"%");
			return (List<Professor>) q.getResultList();
		} catch (NoResultException e) {
			return null;
		}

	}
}
