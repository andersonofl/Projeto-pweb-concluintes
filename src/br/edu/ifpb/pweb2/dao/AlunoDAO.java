package br.edu.ifpb.pweb2.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.edu.ifpb.pweb2.model.Aluno;

public class AlunoDAO extends DAO<Long,Aluno>{
        
    
        
        @SuppressWarnings("unchecked")
	public List<Aluno> getAlunosByNome(String nome) {
		try {
			Query q = super.entityManager.createQuery(
					"Select a from Aluno a where lower(a.nome) like lower(:nome)");
			q.setParameter("nome", "%"+nome+"%");
			return (List<Aluno>) q.getResultList();
		} catch (NoResultException e) {
			return null;
		}

	}
}
