/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.pweb2.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import br.edu.ifpb.pweb2.dao.AlunoDAO;
import br.edu.ifpb.pweb2.dao.ProfessorDAO;
import br.edu.ifpb.pweb2.model.Aluno;
import br.edu.ifpb.pweb2.model.Professor;

/**
 *
 * @author Anderson Leal
 */
@ManagedBean
@ViewScoped
public class AlunoBean extends GenericBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private AlunoDAO dao;
    private ProfessorDAO professorDao;
    private Aluno aluno = new Aluno(); 
    private List<Professor> professores;
    private List<Aluno> alunos;
    private boolean update;

	@PostConstruct
    private void init() {
        dao = new AlunoDAO();
        professorDao = new ProfessorDAO();
        professores = professorDao.findAll();
        alunos = dao.findAll();
        update = false;
    }
    	
	public List<Aluno> completeAluno(String query) {
        List<Aluno> filtrado = new ArrayList<Aluno>();
                  
        for(Aluno alu: alunos){
      	  if(alu.getNome().toLowerCase().startsWith(query)) {
          	  filtrado.add(alu);
            }
        }
         
        return filtrado;
  }
	
    public List<Professor> completeProfessor(String query) {
          List<Professor> professoresFiltrado = new ArrayList<Professor>();
                    
          for(Professor prof: professores){
        	  if(prof.getNome().toLowerCase().startsWith(query)) {
            	  professoresFiltrado.add(prof);
              }
          }
           
          return professoresFiltrado;
    }
    
    public void onRowSelect(SelectEvent event){
        aluno = (Aluno)event.getObject();
    }
    
    public String salvar() {

           dao.beginTransaction();
            
           if(update)
            dao.update(aluno);
           else
        	   dao.save(aluno);
            	
            dao.commit();
    
    	return "listarAlunos?faces-redirect=true";
    }
    
    public String editar() {
    	if(aluno == null){
    		addError("Selecione um aluno");
    		return null;
    	}
    	
	   	return "cadastro_alunos?faces-redirect=true&matricula="+aluno.getMatricula();
    }
    
    public void carregaAluno(){
    	
    	if(aluno.getMatricula() != null){
    		aluno = dao.getById(aluno.getMatricula());
    		update = true;
    	}
    }

    
    public void apagar(ActionEvent event) {
        dao.beginTransaction();
        dao.delete(aluno);
        dao.commit();
        setAlunos(dao.findAll());
    }
    
    public List<Aluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<Aluno> alunos) {
        this.alunos = alunos;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

	public List<Professor> getProfessores() {
		return professores;
	}


	public void setProfessores(List<Professor> professores) {
		this.professores = professores;
	}
}
