/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.pweb2.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import br.edu.ifpb.pweb2.dao.ProcessoConclusaoDAO;
import br.edu.ifpb.pweb2.dao.ProcessoEstagioDAO;
import br.edu.ifpb.pweb2.model.Aluno;
import br.edu.ifpb.pweb2.model.ProcessoConclusao;
import br.edu.ifpb.pweb2.model.ProcessoEstagio;

/**
 *
 * @author andersonleal
 */
@ManagedBean
@ViewScoped
public class ProcessoConclusaoBean extends GenericBean{
	
    private ProcessoConclusaoDAO dao;
    private ProcessoConclusao processo;
    private List<ProcessoConclusao> processos;
    private List<ProcessoEstagio> estagiosOK;
    private Integer filtro;
    private boolean editar;
    
    
    @PostConstruct
    private void init() {
        dao = new ProcessoConclusaoDAO();
        processo = new ProcessoConclusao();
        editar = false;
        ProcessoEstagioDAO estagio = new ProcessoEstagioDAO();
        estagiosOK = estagio.getAlunosConclusao();
    }
  
       
    public List<Aluno> completeAluno(String query) {
        List<Aluno> filtrado = new ArrayList<Aluno>();
                  
        for(ProcessoEstagio e: estagiosOK){
      	  if(e.getAluno().getNome().toLowerCase().startsWith(query)) {
          	  filtrado.add(e.getAluno());
            }
        }
         
        return filtrado;
    }
    
    public void carregaLista(){
    	processos = dao.findAll();
    }
    
    public void finalizacao(){
//    	processos = dao.getAlunosCoordenacao();
    }
   
    public void loadProcesso() {
    	if(processo.getNumero()!=null)
    		processo = dao.getById(processo.getNumero());
    }
        
    public void filtrar(ActionEvent e){
//    	switch(filtro){
//    	case 0:
//    		this.loadTodos();
//    		break;
//    	case 1:
//    		this.protocolo();
//    		break;
//    	case 2:
//    		this.coordenacao();
//    		break;
//    	case 3:
//    		this.orientador();
//    		break;
//    	case 4:
//    		this.retornoCoordenacao();
//    		break;
//    	case 5:
//    		this.coordenacaoEstagio();
//    		break;
//    	case 6:
//    		this.finalizadoProcesso();
//    		break;
//    	}
    }
    
    public String editar() {
        editar = true;
        return "lancamento_conclusao?faces-redirect=true&numero="+processo.getNumero();
    }
           
    public void salvar(ActionEvent event) {
        
        try {
            dao.beginTransaction();
            if(editar)
            	dao.update(processo);
            else
            	dao.save(processo);
            dao.commit();
            addMessage("Cadastrado com sucesso.");
        } catch (Exception e) {
        	e.getStackTrace();
            addError("Error ao salvar.");
        }
        
    }

	public Integer getFiltro() {
		return filtro;
	}

	public void setFiltro(Integer filtro) {
		this.filtro = filtro;
	}


	public ProcessoConclusao getProcesso() {
		return processo;
	}


	public void setProcesso(ProcessoConclusao processo) {
		this.processo = processo;
	}


	public List<ProcessoConclusao> getProcessos() {
		return processos;
	}


	public void setProcessos(List<ProcessoConclusao> processos) {
		this.processos = processos;
	}

	
}
